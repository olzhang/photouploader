var http = require('http');
var url = require('url');

/**
 * starts the application
 * @param route: function from the route module 
 */
function startServer(route, handlers) {
	/**
	 * handles the requests 
	 * @param req: request
	 * @param res: response
	 */
	function onRequest(req, res) {
		
		//parses the request URL for path names 
		var pathname = url.parse(req.url).pathname;
		console.log("Request for "+pathname+" has been recieved");
		
		route(pathname, handlers, res, req);
	}
	
	http.createServer(onRequest).listen(8888);
	console.log('Server running at http://localhost:8888');
}

exports.startServer = startServer;



/**
 * New node file
 */

/**
 * @param pathname: String, pathname; corresponds to the start or upload functions 
 */
function route(pathname, handlers, res, req) {
	console.log("about to route request for "+pathname);
	if(typeof handlers[pathname] === 'function') {
		handlers[pathname](res, req);
	} else {
		console.log('no handler found for '+pathname);
		res.writeHead(404, {'Content-Type': 'text/plain'});
		res.write('404 Error not found');
		res.end();
	}
}

exports.route = route;
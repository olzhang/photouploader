/**
 * New node file
 */

var start = require('./server');
var router = require('./router');
var requestHandlers = require('./requestHandlers');

var handlers = {};
handlers["/"] = requestHandlers.start;
handlers['/start'] = requestHandlers.start;
handlers['/upload'] = requestHandlers.upload;
handlers['/show'] = requestHandlers.show;

//starts app
start.startServer(router.route, handlers);

//maps the pathnames to the corresponding requests as an Object




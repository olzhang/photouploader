/**
 * New node file
 */


var querystring = require("querystring");
var fs = require("fs");
var formidable = require("formidable");
/**
 * handles the 'start' pathname. Corresponds to the GET method
 * @param res
 */
var FILE_LOC = "/Program Files/Enide Studio/ws/PhotoUploader/tmp/test.png";

function start(res, req) {
	console.log("the start handler was called");
	
	var html = '<html>' +
					'<head>' +
						'<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />' +
						'<title>Photo Uploader</title>' +
					'</head>' +
					'<body>' +
						'<form action="/upload" enctype="multipart/form-data" method="post">' +
							'<input type="file" name="upload" multiple>' +
							'<input type="submit" value="show me dat pic" />' +
						'</form>' +
					'</body>' +
				'</html>';
	
	res.writeHead(200, {'Content-Type': 'text/html'});
	res.write(html);
	res.end();
}

/**
 * handles the 'upload' pathname. Corresponds to the POST method
 */
function upload(res, req) {
	console.log("the upload handler was called");
	var form = new formidable.IncomingForm();
	console.log("about to parse");
	
	form.parse(req, function(err, fields, files) {
		console.log("parsing finished");
		
		fs.rename(files.upload.path, FILE_LOC, function(err) {
			if(err) {
				fs.unlink(FILE_LOC);
				fs.rename(files.upload.path, FILE_LOC);
			}
		});
		
		res.writeHead(200, {'Content-Type': 'text/html'});
		res.write("image recieved: <br/>");
		res.write("<image src=/show />");
		res.end();
	});
}

function show(res, req) {
	fs.readFile(FILE_LOC, "binary", function(err, file) {
		if(err) {
			res.writeHead(500, {'Content-Type': 'text/plain'});
			res.write("500 "+err+"\n");
			res.end();
		} else {
			res.writeHead(200, {'Content-Type': 'image/png'});
			res.write(file, "binary");
			res.end();
		}
	});
}

exports.start = start;
exports.upload = upload;
exports.show = show;